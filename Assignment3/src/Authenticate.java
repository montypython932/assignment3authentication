/*
 * Written by Phillip Hall, June 2014
 * Program takes input for username and password
 * and account type as input and authenticates it
 * 
 * PSEUDOCODE:
 * 1. Take username and password as input, so long as there are less than three failed attempts
 * 2. Check to see if there are more than three failed attempts, display "Contact Administrator" if so
 * 3. Display drop down box to choose account type and keep it until the user selects the right account
 * 4. Display different message depending of the account type
 */

import javax.swing.JOptionPane;

public class Authenticate {

	private static final String USERNAME = "Phil";
	private static final String PASSWORD = "12345";
	private static final Object[] ACCOUNT = { "Administrator", "Student", "Faculty" };
	
	public static void main(String[] args) {
		int failedAttemptCounter = 0;
		String correctAccountType = (String) ACCOUNT[0];
		String uName = "";
		String pWord = "";
		String accountType = "";
		
		//Loop that takes username and password as input and checks them
		while (!pWord.equalsIgnoreCase(PASSWORD) && failedAttemptCounter < 3) {
			if (!uName.equalsIgnoreCase(USERNAME)) {
				uName = JOptionPane.showInputDialog("Enter Username");
				if (uName.equalsIgnoreCase(USERNAME)) {
					failedAttemptCounter--;
				}
			}
			else if (!pWord.equalsIgnoreCase(PASSWORD)) {
				pWord = JOptionPane.showInputDialog("Enter Password");
			}
			failedAttemptCounter++;
		}
		//if username/password entered incorrectly 3 times, contact administrator
		if (failedAttemptCounter == 3) {
			JOptionPane.showMessageDialog(null, "Contact Administrator");
		}
		//Loop that displays drop down to choose account type, keeps displaying until correct one is selected
		while (!accountType.equalsIgnoreCase(correctAccountType) && failedAttemptCounter < 3) {
			accountType = (String) JOptionPane.showInputDialog(null, "Select Your Account Type","AccountType", JOptionPane.QUESTION_MESSAGE, null, ACCOUNT, ACCOUNT[0]);
			if (accountType.equalsIgnoreCase(correctAccountType)) {
				//displays different greetings depending on accountType
				switch (accountType) {
				case "Administrator":
					JOptionPane.showMessageDialog(null, "Welcome Administrator!");
					break;
				case "Student":
					JOptionPane.showMessageDialog(null, "Welcome Student!");
					break;
				case "Faculty":
					JOptionPane.showMessageDialog(null, "Welcome Faculty!");
					break;
				}
			}
		}
	}
}
